# Velká práce

Projekt do předmětu SSP - velká práce. Django projekt na téma vizualizace grafů a technická analýza kryptoměnového trhu.

## Instalace

1. Instalace balíčků
    ```
    pip install -r requirements.txt
    ```
1. Run
    ```
    python manage.py runserver
    ```

Ke korektnímu spuštění je nutné doplnit do zrojových souborů api klíče a přístupové údaje k databázi. Tyto údaje nesmí být zveřejněny.

