import requests
from bs4 import BeautifulSoup
import re


def remove_lower(text):
    return re.sub('[a-z ]+', '', text)


r = requests.get('https://cryptologos.cc/logos/')
soup = BeautifulSoup(r.content, 'html.parser')
urls = []
logo_names = []
for a in soup.find_all('a', href=True):
    urls.append(a['href'])

del urls[1::2]  # delete every second url, because every second is .SVG

# Correct mistakes and add logo name to logo_names array
for logo_name in soup.findAll('a'):
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "USDBUSD":
        logo_names.append("BUSD")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "CBNB":
        logo_names.append("BNB")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "CBCH":
        logo_names.append("BCH")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "OSEOS":
        logo_names.append("EOS")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "CETC":
        logo_names.append("ETC")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "RPXRP":
        logo_names.append("XRP")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "FFUN":
        logo_names.append("FUN")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "RONTRX":
        logo_names.append("TRX")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "NUSSEDLEOLEO":
        logo_names.append("LEO")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "THT":
        logo_names.append("HT")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "THEDG":
        logo_names.append("HEDG")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "OTAMIOTA":
        logo_names.append("MIOTA")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "BDGB":
        logo_names.append("DGB")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "KBOKB":
        logo_names.append("OKB")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "TXFTT":
        logo_names.append("FTT")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "ATBAT":
        logo_names.append("BAT")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "CVET":
        logo_names.append("VET")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "SPAX":
        logo_names.append("PAX")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "GBTG":
        logo_names.append("BTG")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "HHBAR":
        logo_names.append("HBAR")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "NTSNX":
        logo_names.append("SNX")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "CONICX":
        logo_names.append("ICX")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "USDTUSD":
        logo_names.append("TUSD")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "HETATHETA":
        logo_names.append("THETA")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "CENJ":
        logo_names.append("ENJ")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "NKNC":
        logo_names.append("KNC")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "TZB":
        logo_names.append("ZB")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "DAIDAI":
        logo_names.append("DAI")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "DBCD":
        logo_names.append("BCD")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "GOOMG":
        logo_names.append("OMG")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "CMONA":
        logo_names.append("MONA")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "COMCO":
        logo_names.append("MCO")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "DAODGD":
        logo_names.append("DGD")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "NCKB":
        logo_names.append("CKB")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "CSKCS":
        logo_names.append("KCS")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "BBCCABBC":
        logo_names.append("ABBC")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "TBTT":
        logo_names.append("BTT")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "NMATIC":
        logo_names.append("MATIC")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "SBTS":
        logo_names.append("BTS")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "NSEELE":
        logo_names.append("SEELE")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "SYS":
        logo_names.append("VSYS")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "CHC":
        logo_names.append("HC")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "SCMAID":
        logo_names.append("MAID")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "CRDD":
        logo_names.append("RDD")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "OSTIOST":
        logo_names.append("IOST")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "AXGPAXG":
        logo_names.append("PAXG")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "CNRCN":
        logo_names.append("RCN")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "LF":
        logo_names.append("ELF")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "AXWAXP":
        logo_names.append("WAXP")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "LPOWR":
        logo_names.append("POWR")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "CWICC":
        logo_names.append("WICC")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "XNPXS":
        logo_names.append("NPXS")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "XCGXC":
        logo_names.append("GXC")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "RLCRLC":
        logo_names.append("RLC")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "CTOMO":
        logo_names.append("TOMO")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "POCEAN":
        logo_names.append("OCEAN")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "KADK":
        logo_names.append("ADK")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "IVXPIVX":
        logo_names.append("PIVX")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "NLOOM":
        logo_names.append("LOOM")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "TXIOTX":
        logo_names.append("IOTX")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "BCHSB":
        logo_names.append("CHSB")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "ASHQASH":
        logo_names.append("QASH")
        continue
    if remove_lower(logo_name.string).split(".", 1)[0][1:] == "FMOF":
        logo_names.append("MOF")
        continue

    logo_names.append(remove_lower(logo_name.string).split(".", 1)[0][1:])
del logo_names[1::2]  # delete every second logo_name, because every second is .SVG
