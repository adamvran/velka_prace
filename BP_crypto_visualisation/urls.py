"""BP_crypto_visualisation URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

import BP_crypto_visualisation.views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', BP_crypto_visualisation.views.index, name='index'),
    path("register", BP_crypto_visualisation.views.register_request, name="register"),
    path("login", BP_crypto_visualisation.views.login_request, name="login"),
    path("logout", BP_crypto_visualisation.views.logout_request, name="logout"),
    path("binance_api/", BP_crypto_visualisation.views.binance_api, name="binance_api"),
    path("binance_api_live/", BP_crypto_visualisation.views.binance_api_live, name="binance_api_live"),
    path("coinbase_api/", BP_crypto_visualisation.views.coinbase_api, name="coinbase_api"),
    path('get_candlestick_data_binance/symbol=<str:symbol>', BP_crypto_visualisation.views.get_candlestick_data_binance, name='get_candlestick_data_binance'),
    path('get_candlestick_data_coinbase/symbol=<str:symbol>', BP_crypto_visualisation.views.get_candlestick_data_coinbase, name='get_candlestick_data_coinbase'),
]
