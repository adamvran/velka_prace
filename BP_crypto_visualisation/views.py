import random

from django.shortcuts import redirect, render
import mysql.connector
from mysql.connector import Error
from .forms import NewUserForm
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from .logos import logo_names, urls
import json
from django.http import HttpResponse
from binance.client import Client
import cbpro
import requests

import datetime


def logout_request(request):
    logout(request)
    messages.info(request, "You have successfully logged out.")
    return redirect("index")


def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {username}.")
                return redirect("index")
            else:
                messages.error(request, "Invalid username or password.")
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request=request, template_name="login.html", context={"login_form": form})


def get_label_names():
    try:
        connection = mysql.connector.connect(host='<ip adress of host>',
                                             database='binance',
                                             user='<username>',
                                             password='<password>',
                                             port=3307)
        if connection.is_connected():
            cursor = connection.cursor()
            cursor.execute("select database();")
            cursor.fetchone()
    except Error as e:
        print("Error while connecting to MySQL", e)

    statement = "select distinct label_name from product where status = 'trading'"
    cursor.execute(statement)
    label_names = cursor.fetchall()
    connection.close()
    label_names_correct = []
    for label_name in label_names:
        label_names_correct.append(label_name[0])

    return label_names_correct


def register_request(request):
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Registration successful.")
            return redirect("index")
        messages.error(request, "Unsuccessful registration. Invalid information.")
    form = NewUserForm()
    return render(request=request, template_name="register.html", context={"register_form": form})


def get_url_logo_of_coin_by_database(database):
    correct_database = database.split("_", 2)[2]
    left_found = False
    right_found = False
    for logo_name in logo_names:
        if not left_found:
            if correct_database.upper().startswith(logo_name.upper()):
                if logo_name != "":
                    left_found = True
                    left = urls[logo_names.index(logo_name)]

        if not right_found:
            if correct_database.upper().endswith(logo_name.upper()):
                if logo_name != "":
                    right_found = True
                    right = urls[logo_names.index(logo_name)]

        if left_found and right_found:
            return [left, right]

    if left_found:
        return [left, "https://pngimg.com/uploads/question_mark/question_mark_PNG68.png"]

    if right_found:
        return ["https://pngimg.com/uploads/question_mark/question_mark_PNG68.png", right]

    return ["https://pngimg.com/uploads/question_mark/question_mark_PNG68.png",
            "https://pngimg.com/uploads/question_mark/question_mark_PNG68.png"]


def index(request):
    print(logo_names)
    peak_valley_data_begin = []
    peak_valley_data_end = []
    peak_valley_data_type = []
    peak_valley_data_begin2 = []
    peak_valley_data_end2 = []
    peak_valley_data_type2 = []
    peak_valley_data_begin3 = []
    peak_valley_data_end3 = []
    peak_valley_data_type3 = []
    peak_valley_data_begin4 = []
    peak_valley_data_end4 = []
    peak_valley_data_type4 = []
    logos = []
    # DB connection and get CANDLES DATA
    try:
        connection = mysql.connector.connect(host='<ip adress of host>',
                                             database='binance_pair_btceur',
                                             user='<username>',
                                             password='<password>',
                                             port=3307)
        if connection.is_connected():
            db_Info = connection.get_server_info()
            print("Connected to MySQL Server version ", db_Info)
            cursor = connection.cursor()
            cursor.execute("select database();")
            pair_data = cursor.fetchone()
            print("You're connected to database1: ", pair_data[0], "\n")

    except Error as e:
        print("Error while connecting to MySQL", e)

    cursor.execute("show databases")
    result = cursor.fetchall()

    bad_databases = ['binance', 'crypto', 'performance_schema', 'information_schema', 'mysql', 'sys']
    databases = []
    random.shuffle(result)
    for database in result:
        if len(databases) > 30:
            break
        if not any(item in bad_databases for item in database):
            if '000' not in database[0]:
                databases.append(database[0])

    gainers = dict()

    for database in databases:
        try:
            connection = mysql.connector.connect(host='<ip adress of host>',
                                                 database=database,
                                                 user='<username>',
                                                 password='<password>',
                                                 port=3307)
            if connection.is_connected():
                cursor = connection.cursor()
                cursor.execute("select database();")
                record = cursor.fetchone()
                print("You're connected to database2: ", record[0], "\n")
        except Error as e:
            print("Error while connecting to MySQL", e)

        statement = "select close, datetime from " + database + ".spot_kline order by datetime desc limit 45"
        cursor.execute(statement)
        data = cursor.fetchall()

        if data:
            change = data[0][0] / data[-1][0]
            gainers.update({database: change})

    gainers_sorted = sorted(gainers.items(), key=lambda el: el[1], reverse=True)

    databases = []

    for element in gainers_sorted:
        databases.append(element[0])
        print(element[0] + " || " + str(element[1]))

    pair_names = []

    statement = "select unixtime, open, high, low, close from " + databases[4] + ".spot_kline order by datetime desc " \
                                                                                 "limit 5000"
    print(statement)
    cursor.execute(statement)
    chart_1_data = cursor.fetchall()
    pair_names.append(databases[0].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[5] + ".spot_kline order by datetime desc " \
                                                                                 "limit 5000"
    print(statement)
    cursor.execute(statement)
    chart_2_data = cursor.fetchall()
    pair_names.append(databases[1].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[6] + ".spot_kline order by datetime desc " \
                                                                                 "limit 5000"
    print(statement)
    cursor.execute(statement)
    chart_3_data = cursor.fetchall()
    pair_names.append(databases[2].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[7] + ".spot_kline order by datetime desc " \
                                                                                 "limit 5000"
    print(statement)
    cursor.execute(statement)
    chart_4_data = cursor.fetchall()
    pair_names.append(databases[3].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[0] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_gainer_1_data = cursor.fetchall()
    pair_names.append(databases[4].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[1] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_gainer_2_data = cursor.fetchall()
    pair_names.append(databases[5].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[2] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_gainer_3_data = cursor.fetchall()
    pair_names.append(databases[6].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[3] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_gainer_4_data = cursor.fetchall()
    pair_names.append(databases[7].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 1] + ".spot_kline order by datetime desc " \
                              "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_looser_1_data = cursor.fetchall()
    pair_names.append(databases[8].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 2] + ".spot_kline order by datetime desc " \
                              "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_looser_2_data = cursor.fetchall()
    pair_names.append(databases[9].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 3] + ".spot_kline order by datetime desc " \
                              "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_looser_3_data = cursor.fetchall()
    pair_names.append(databases[10].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 4] + ".spot_kline order by datetime desc " \
                              "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_looser_4_data = cursor.fetchall()
    pair_names.append(databases[11].split("_", 2)[2].upper())

    for x in range(4, 12):
        for y in range(0, 2):
            logos.append(get_url_logo_of_coin_by_database(databases[x])[y])

    connection.close()

    # DB connection and get PEAK VALLEY DATA
    try:
        connection = mysql.connector.connect(host='<ip adress of host>',
                                             database='binance_pair_btceur',
                                             user='<username>',
                                             password='<password>',
                                             port=3307)
        if connection.is_connected():
            db_Info = connection.get_server_info()
            print("Connected to MySQL Server version ", db_Info)
            cursor = connection.cursor()
            cursor.execute("select database();")
            peak_valley_data = cursor.fetchone()
            print("You're connected to database3: ", peak_valley_data[0], "\n")

    except Error as e:
        print("Error while connecting to MySQL", e)

    statement = "select UNIX_TIMESTAMP(DATE_FORMAT(date_begin, '%Y-%m-%d %T')), UNIX_TIMESTAMP(DATE_FORMAT(date_end, " \
                "'%Y-%m-%d %T')), type from " + databases[4] + ".peak_valley_node where date_begin is not null order " \
                                                               "by date_begin desc limit 10"

    cursor.execute(statement)
    peak_valley_data = cursor.fetchall()

    statement = "select UNIX_TIMESTAMP(DATE_FORMAT(date_begin, '%Y-%m-%d %T')), UNIX_TIMESTAMP(DATE_FORMAT(date_end, " \
                "'%Y-%m-%d %T')), type from " + databases[5] + ".peak_valley_node where date_begin is not null order " \
                                                               "by date_begin desc limit 10"
    cursor.execute(statement)
    peak_valley_data_2 = cursor.fetchall()

    statement = "select UNIX_TIMESTAMP(DATE_FORMAT(date_begin, '%Y-%m-%d %T')), UNIX_TIMESTAMP(DATE_FORMAT(date_end, " \
                "'%Y-%m-%d %T')), type from " + databases[6] + ".peak_valley_node where date_begin is not null order " \
                                                               "by date_begin desc limit 10"
    cursor.execute(statement)
    peak_valley_data_3 = cursor.fetchall()

    statement = "select UNIX_TIMESTAMP(DATE_FORMAT(date_begin, '%Y-%m-%d %T')), UNIX_TIMESTAMP(DATE_FORMAT(date_end, " \
                "'%Y-%m-%d %T')), type from " + databases[7] + ".peak_valley_node where date_begin is not null order " \
                                                               "by date_begin desc limit 10"
    cursor.execute(statement)
    peak_valley_data_4 = cursor.fetchall()

    connection.close()

    for record in peak_valley_data:
        peak_valley_data_begin.append(int(record[0]))
        peak_valley_data_end.append(int(record[1]))
        peak_valley_data_type.append(1 if record[2] == 'peak' else 0)  # PEAK = 1, VALLEY = 0
    for record in peak_valley_data_2:
        peak_valley_data_begin2.append(int(record[0]))
        peak_valley_data_end2.append(int(record[1]))
        peak_valley_data_type2.append(1 if record[2] == 'peak' else 0)  # PEAK = 1, VALLEY = 0
    for record in peak_valley_data_3:
        peak_valley_data_begin3.append(int(record[0]))
        peak_valley_data_end3.append(int(record[1]))
        peak_valley_data_type3.append(1 if record[2] == 'peak' else 0)  # PEAK = 1, VALLEY = 0
    for record in peak_valley_data_4:
        peak_valley_data_begin4.append(int(record[0]))
        peak_valley_data_end4.append(int(record[1]))
        peak_valley_data_type4.append(1 if record[2] == 'peak' else 0)  # PEAK = 1, VALLEY = 0

    print(peak_valley_data_type)

    return render(request, 'index.html', {'chart_1_data': json.dumps(chart_1_data),
                                          'chart_2_data': json.dumps(chart_2_data),
                                          'chart_3_data': json.dumps(chart_3_data),
                                          'chart_4_data': json.dumps(chart_4_data),
                                          'gainer_1_data': json.dumps(chart_gainer_1_data),
                                          'gainer_2_data': json.dumps(chart_gainer_2_data),
                                          'gainer_3_data': json.dumps(chart_gainer_3_data),
                                          'gainer_4_data': json.dumps(chart_gainer_4_data),
                                          'looser_1_data': json.dumps(chart_looser_1_data),
                                          'looser_2_data': json.dumps(chart_looser_2_data),
                                          'looser_3_data': json.dumps(chart_looser_3_data),
                                          'looser_4_data': json.dumps(chart_looser_4_data),
                                          'peak_valley_begin': peak_valley_data_begin,
                                          'peak_valley_end': peak_valley_data_end,
                                          'peak_valley_type': peak_valley_data_type,
                                          'peak_valley_begin2': peak_valley_data_begin2,
                                          'peak_valley_end2': peak_valley_data_end2,
                                          'peak_valley_type2': peak_valley_data_type2,
                                          'peak_valley_begin3': peak_valley_data_begin3,
                                          'peak_valley_end3': peak_valley_data_end3,
                                          'peak_valley_type3': peak_valley_data_type3,
                                          'peak_valley_begin4': peak_valley_data_begin4,
                                          'peak_valley_end4': peak_valley_data_end4,
                                          'peak_valley_type4': peak_valley_data_type4,
                                          # 'history_chart_data': json.dumps(history_chart_data),
                                          'logos': logos,
                                          'pair_names': pair_names})


def get_candlestick_data_binance(request, symbol):
    client = Client("<api_key>",
                    "<secret_api_key>")
    candlesticks = client.get_historical_klines(symbol=symbol, interval=Client.KLINE_INTERVAL_1MINUTE, limit=200)
    data = [[candlestick[0], float(candlestick[1]), float(candlestick[2]), float(candlestick[3]), float(candlestick[4])]
            for candlestick in candlesticks]
    response_data = {'data': data}
    return HttpResponse(json.dumps(response_data), content_type='application/json')


def binance_api(request):
    print(logo_names)
    logos = []
    # DB connection and get CANDLES DATA
    try:
        connection = mysql.connector.connect(host='<ip adress of host>',
                                             database='binance_pair_btceur',
                                             user='<username>',
                                             password='<password>',
                                             port=3307)
        if connection.is_connected():
            db_Info = connection.get_server_info()
            print("Connected to MySQL Server version ", db_Info)
            cursor = connection.cursor()
            cursor.execute("select database();")
            pair_data = cursor.fetchone()
            print("You're connected to database1: ", pair_data[0], "\n")

    except Error as e:
        print("Error while connecting to MySQL", e)

    cursor.execute("show databases")
    result = cursor.fetchall()

    bad_databases = ['binance', 'crypto', 'performance_schema', 'information_schema', 'mysql', 'sys']
    databases = []
    random.shuffle(result)
    for database in result:
        if len(databases) > 30:
            break
        if not any(item in bad_databases for item in database):
            if '000' not in database[0]:
                databases.append(database[0])

    # DB connection and get the biggest gainers and loosers

    gainers = dict()

    for database in databases:
        try:
            connection = mysql.connector.connect(host='<ip adress of host>',
                                                 database=database,
                                                 user='<username>',
                                                 password='<password>',
                                                 port=3307)
            if connection.is_connected():
                cursor = connection.cursor()
                cursor.execute("select database();")
                record = cursor.fetchone()
                print("You're connected to database2: ", record[0], "\n")
        except Error as e:
            print("Error while connecting to MySQL", e)

        statement = "select close, datetime from " + database + ".spot_kline order by datetime desc limit 45"
        cursor.execute(statement)
        data = cursor.fetchall()

        if data:
            change = data[0][0] / data[-1][0]
            gainers.update({database: change})

    gainers_sorted = sorted(gainers.items(), key=lambda el: el[1], reverse=True)

    databases = []

    for element in gainers_sorted:
        databases.append(element[0])
        print(element[0] + " || " + str(element[1]))

    pair_names = []

    statement = "select unixtime, open, high, low, close from " + databases[4] + ".spot_kline order by datetime desc " \
                                                                                 "limit 200"
    print(statement)
    cursor.execute(statement)
    chart_1_data = cursor.fetchall()
    pair_names.append(databases[0].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[5] + ".spot_kline order by datetime desc " \
                                                                                 "limit 200"
    print(statement)
    cursor.execute(statement)
    chart_2_data = cursor.fetchall()
    pair_names.append(databases[1].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[6] + ".spot_kline order by datetime desc " \
                                                                                 "limit 200"
    print(statement)
    cursor.execute(statement)
    chart_3_data = cursor.fetchall()
    pair_names.append(databases[2].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[7] + ".spot_kline order by datetime desc " \
                                                                                 "limit 200"
    print(statement)
    cursor.execute(statement)
    chart_4_data = cursor.fetchall()
    pair_names.append(databases[3].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[0] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_gainer_1_data = cursor.fetchall()
    pair_names.append(databases[4].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[1] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_gainer_2_data = cursor.fetchall()
    pair_names.append(databases[5].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[2] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_gainer_3_data = cursor.fetchall()
    pair_names.append(databases[6].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[3] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_gainer_4_data = cursor.fetchall()
    pair_names.append(databases[7].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 1] + ".spot_kline order by datetime desc " \
                              "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_looser_1_data = cursor.fetchall()
    pair_names.append(databases[8].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 2] + ".spot_kline order by datetime desc " \
                              "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_looser_2_data = cursor.fetchall()
    pair_names.append(databases[9].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 3] + ".spot_kline order by datetime desc " \
                              "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_looser_3_data = cursor.fetchall()
    pair_names.append(databases[10].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 4] + ".spot_kline order by datetime desc " \
                              "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_looser_4_data = cursor.fetchall()
    pair_names.append(databases[11].split("_", 2)[2].upper())

    for x in range(4, 12):
        for y in range(0, 2):
            logos.append(get_url_logo_of_coin_by_database(databases[x])[y])

    connection.close()

    return render(request, 'binance_api.html', {'chart_1_data': json.dumps(chart_1_data),
                                                'chart_2_data': json.dumps(chart_2_data),
                                                'chart_3_data': json.dumps(chart_3_data),
                                                'chart_4_data': json.dumps(chart_4_data),
                                                'gainer_1_data': json.dumps(chart_gainer_1_data),
                                                'gainer_2_data': json.dumps(chart_gainer_2_data),
                                                'gainer_3_data': json.dumps(chart_gainer_3_data),
                                                'gainer_4_data': json.dumps(chart_gainer_4_data),
                                                'looser_1_data': json.dumps(chart_looser_1_data),
                                                'looser_2_data': json.dumps(chart_looser_2_data),
                                                'looser_3_data': json.dumps(chart_looser_3_data),
                                                'looser_4_data': json.dumps(chart_looser_4_data),
                                                # 'history_chart_data': json.dumps(history_chart_data),
                                                'logos': logos,
                                                'pair_names': pair_names})


def get_candlestick_data_coinbase(request, symbol):
    c = cbpro.PublicClient()

    end = datetime.datetime.now()
    start = end - datetime.timedelta(hours=5)
    granularity = 60
    candles = c.get_product_historic_rates(symbol, start, end, granularity)
    response_data = {'data': candles}

    return HttpResponse(json.dumps(response_data), content_type='application/json')


def get_label_name_from_pair_name(pair_name):
    label_names = get_label_names()
    for label_name in label_names:
        if pair_name.upper().startswith(label_name.split("-")[0]):
            if pair_name.upper().endswith(label_name.split("-")[1]):
                return label_name


def get_all_products_from_coinbase():
    products = []
    api_key = '<api_key>'
    headers = {
        'Authorization': 'Bearer ' + api_key
    }

    response = requests.get('https://api.exchange.coinbase.com/products', headers=headers)
    data = response.json()

    for element in data:
        products.append(element["id"])

    return products


def coinbase_api(request):
    logos = []

    bad_databases = ['binance', 'crypto', 'performance_schema', 'information_schema', 'mysql', 'sys']
    databases = []

    common_label_names = set(get_all_products_from_coinbase()).intersection(get_label_names())
    databases_common_label_names = []
    for common_label_name in common_label_names:
        databases_common_label_names.append("binance_pair_" + common_label_name.split("-")[0].lower() +
                                            common_label_name.split("-")[1].lower())

    print(databases_common_label_names)
    random.shuffle(databases_common_label_names)

    for database in databases_common_label_names:
        if len(databases) > 30:
            break
        if not any(item in bad_databases for item in database):
            if '000' not in database[0]:
                databases.append(database)

    # DB connection and get the biggest gainers and loosers

    gainers = dict()

    for database in databases:
        try:
            connection = mysql.connector.connect(host='<ip adress of host>',
                                                 database=database,
                                                 user='<username>',
                                                 password='<password>',
                                                 port=3307)
            if connection.is_connected():
                cursor = connection.cursor()
                cursor.execute("select database();")
                record = cursor.fetchone()
                print("You're connected to database2: ", record[0], "\n")
        except Error as e:
            print("Error while connecting to MySQL", e)

        statement = "select close, datetime from " + database + ".spot_kline order by datetime desc limit 45"
        cursor.execute(statement)
        data = cursor.fetchall()

        if data:
            change = data[0][0] / data[-1][0]
            gainers.update({database: change})

    gainers_sorted = sorted(gainers.items(), key=lambda el: el[1], reverse=True)

    databases = []

    for element in gainers_sorted:
        databases.append(element[0])
        print(element[0] + " || " + str(element[1]))

    pair_names = []
    label_names = []

    statement = "select unixtime, open, high, low, close from " + databases[4] + ".spot_kline order by datetime desc " \
                                                                                 "limit 200"
    cursor.execute(statement)
    chart_1_data = cursor.fetchall()
    pair_names.append(databases[0].split("_", 2)[2].upper())
    label_names.append(get_label_name_from_pair_name(pair_names[-1]))

    statement = "select unixtime, open, high, low, close from " + databases[5] + ".spot_kline order by datetime desc " \
                                                                                 "limit 200"
    cursor.execute(statement)
    chart_2_data = cursor.fetchall()
    pair_names.append(databases[1].split("_", 2)[2].upper())
    label_names.append(get_label_name_from_pair_name(pair_names[-1]))

    statement = "select unixtime, open, high, low, close from " + databases[6] + ".spot_kline order by datetime desc " \
                                                                                 "limit 200"
    cursor.execute(statement)
    chart_3_data = cursor.fetchall()
    pair_names.append(databases[2].split("_", 2)[2].upper())
    label_names.append(get_label_name_from_pair_name(pair_names[-1]))

    statement = "select unixtime, open, high, low, close from " + databases[7] + ".spot_kline order by datetime desc " \
                                                                                 "limit 200"
    cursor.execute(statement)
    chart_4_data = cursor.fetchall()
    pair_names.append(databases[3].split("_", 2)[2].upper())
    label_names.append(get_label_name_from_pair_name(pair_names[-1]))

    statement = "select unixtime, open, high, low, close from " + databases[0] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    cursor.execute(statement)
    chart_gainer_1_data = cursor.fetchall()
    pair_names.append(databases[4].split("_", 2)[2].upper())
    label_names.append(get_label_name_from_pair_name(pair_names[-1]))

    statement = "select unixtime, open, high, low, close from " + databases[1] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    cursor.execute(statement)
    chart_gainer_2_data = cursor.fetchall()
    pair_names.append(databases[5].split("_", 2)[2].upper())
    label_names.append(get_label_name_from_pair_name(pair_names[-1]))

    statement = "select unixtime, open, high, low, close from " + databases[2] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    cursor.execute(statement)
    chart_gainer_3_data = cursor.fetchall()
    pair_names.append(databases[6].split("_", 2)[2].upper())
    label_names.append(get_label_name_from_pair_name(pair_names[-1]))

    statement = "select unixtime, open, high, low, close from " + databases[3] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    cursor.execute(statement)
    chart_gainer_4_data = cursor.fetchall()
    pair_names.append(databases[7].split("_", 2)[2].upper())
    label_names.append(get_label_name_from_pair_name(pair_names[-1]))

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 1] + ".spot_kline order by datetime desc " \
                              "limit 45"
    cursor.execute(statement)
    chart_looser_1_data = cursor.fetchall()
    pair_names.append(databases[8].split("_", 2)[2].upper())
    label_names.append(get_label_name_from_pair_name(pair_names[-1]))

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 2] + ".spot_kline order by datetime desc " \
                              "limit 45"
    cursor.execute(statement)
    chart_looser_2_data = cursor.fetchall()
    pair_names.append(databases[9].split("_", 2)[2].upper())
    label_names.append(get_label_name_from_pair_name(pair_names[-1]))

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 3] + ".spot_kline order by datetime desc " \
                              "limit 45"
    cursor.execute(statement)
    chart_looser_3_data = cursor.fetchall()
    pair_names.append(databases[10].split("_", 2)[2].upper())
    label_names.append(get_label_name_from_pair_name(pair_names[-1]))

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 4] + ".spot_kline order by datetime desc " \
                              "limit 45"
    cursor.execute(statement)
    chart_looser_4_data = cursor.fetchall()
    pair_names.append(databases[11].split("_", 2)[2].upper())
    label_names.append(get_label_name_from_pair_name(pair_names[-1]))

    for x in range(4, 12):
        for y in range(0, 2):
            logos.append(get_url_logo_of_coin_by_database(databases[x])[y])

    connection.close()

    return render(request, 'coinbase_api.html', {'chart_1_data': json.dumps(chart_1_data),
                                                 'chart_2_data': json.dumps(chart_2_data),
                                                 'chart_3_data': json.dumps(chart_3_data),
                                                 'chart_4_data': json.dumps(chart_4_data),
                                                 'gainer_1_data': json.dumps(chart_gainer_1_data),
                                                 'gainer_2_data': json.dumps(chart_gainer_2_data),
                                                 'gainer_3_data': json.dumps(chart_gainer_3_data),
                                                 'gainer_4_data': json.dumps(chart_gainer_4_data),
                                                 'looser_1_data': json.dumps(chart_looser_1_data),
                                                 'looser_2_data': json.dumps(chart_looser_2_data),
                                                 'looser_3_data': json.dumps(chart_looser_3_data),
                                                 'looser_4_data': json.dumps(chart_looser_4_data),
                                                 # 'history_chart_data': json.dumps(history_chart_data),
                                                 'logos': logos,
                                                 'pair_names': pair_names,
                                                 'label_names': label_names,
                                                 'common_label_names': common_label_names})


def binance_api_live(request):
    logos = []
    # DB connection and get CANDLES DATA
    try:
        connection = mysql.connector.connect(host='<ip adress of host>',
                                             database='binance_pair_btceur',
                                             user='<username>',
                                             password='<password>',
                                             port=3307)
        if connection.is_connected():
            db_Info = connection.get_server_info()
            print("Connected to MySQL Server version ", db_Info)
            cursor = connection.cursor()
            cursor.execute("select database();")
            pair_data = cursor.fetchone()
            print("You're connected to database1: ", pair_data[0], "\n")

    except Error as e:
        print("Error while connecting to MySQL", e)

    cursor.execute("show databases")
    result = cursor.fetchall()

    bad_databases = ['binance', 'crypto', 'performance_schema', 'information_schema', 'mysql', 'sys']
    databases = []
    random.shuffle(result)
    for database in result:
        if len(databases) > 30:
            break
        if not any(item in bad_databases for item in database):
            if '000' not in database[0]:
                databases.append(database[0])

    # DB connection and get the biggest gainers and loosers

    gainers = dict()

    for database in databases:
        try:
            connection = mysql.connector.connect(host='<ip adress of host>',
                                                 database=database,
                                                 user='<username>',
                                                 password='<password>',
                                                 port=3307)
            if connection.is_connected():
                cursor = connection.cursor()
                cursor.execute("select database();")
                record = cursor.fetchone()
                print("You're connected to database2: ", record[0], "\n")
        except Error as e:
            print("Error while connecting to MySQL", e)

        statement = "select close, datetime from " + database + ".spot_kline order by datetime desc limit 45"
        cursor.execute(statement)
        data = cursor.fetchall()

        if data:
            change = data[0][0] / data[-1][0]
            gainers.update({database: change})

    gainers_sorted = sorted(gainers.items(), key=lambda el: el[1], reverse=True)

    databases = []

    for element in gainers_sorted:
        databases.append(element[0])
        print(element[0] + " || " + str(element[1]))

    pair_names = []

    statement = "select unixtime, open, high, low, close from " + databases[4] + ".spot_kline order by datetime desc " \
                                                                                 "limit 200"
    print(statement)
    cursor.execute(statement)
    chart_1_data = cursor.fetchall()
    pair_names.append(databases[0].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[5] + ".spot_kline order by datetime desc " \
                                                                                 "limit 200"
    print(statement)
    cursor.execute(statement)
    chart_2_data = cursor.fetchall()
    pair_names.append(databases[1].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[6] + ".spot_kline order by datetime desc " \
                                                                                 "limit 200"
    print(statement)
    cursor.execute(statement)
    chart_3_data = cursor.fetchall()
    pair_names.append(databases[2].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[7] + ".spot_kline order by datetime desc " \
                                                                                 "limit 200"
    print(statement)
    cursor.execute(statement)
    chart_4_data = cursor.fetchall()
    pair_names.append(databases[3].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[0] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_gainer_1_data = cursor.fetchall()
    pair_names.append(databases[4].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[1] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_gainer_2_data = cursor.fetchall()
    pair_names.append(databases[5].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[2] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_gainer_3_data = cursor.fetchall()
    pair_names.append(databases[6].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[3] + ".spot_kline order by datetime desc " \
                                                                                 "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_gainer_4_data = cursor.fetchall()
    pair_names.append(databases[7].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 1] + ".spot_kline order by datetime desc " \
                              "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_looser_1_data = cursor.fetchall()
    pair_names.append(databases[8].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 2] + ".spot_kline order by datetime desc " \
                              "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_looser_2_data = cursor.fetchall()
    pair_names.append(databases[9].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 3] + ".spot_kline order by datetime desc " \
                              "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_looser_3_data = cursor.fetchall()
    pair_names.append(databases[10].split("_", 2)[2].upper())

    statement = "select unixtime, open, high, low, close from " + databases[
        len(databases) - 4] + ".spot_kline order by datetime desc " \
                              "limit 45"
    print(statement)
    cursor.execute(statement)
    chart_looser_4_data = cursor.fetchall()
    pair_names.append(databases[11].split("_", 2)[2].upper())

    for x in range(4, 12):
        for y in range(0, 2):
            logos.append(get_url_logo_of_coin_by_database(databases[x])[y])

    connection.close()

    return render(request, 'binance_api_live.html', {'chart_1_data': json.dumps(chart_1_data),
                                                     'chart_2_data': json.dumps(chart_2_data),
                                                     'chart_3_data': json.dumps(chart_3_data),
                                                     'chart_4_data': json.dumps(chart_4_data),
                                                     'gainer_1_data': json.dumps(chart_gainer_1_data),
                                                     'gainer_2_data': json.dumps(chart_gainer_2_data),
                                                     'gainer_3_data': json.dumps(chart_gainer_3_data),
                                                     'gainer_4_data': json.dumps(chart_gainer_4_data),
                                                     'looser_1_data': json.dumps(chart_looser_1_data),
                                                     'looser_2_data': json.dumps(chart_looser_2_data),
                                                     'looser_3_data': json.dumps(chart_looser_3_data),
                                                     'looser_4_data': json.dumps(chart_looser_4_data),
                                                     # 'history_chart_data': json.dumps(history_chart_data),
                                                     'logos': logos,
                                                     'pair_names': pair_names})
