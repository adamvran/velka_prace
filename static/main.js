window.scrollTo(0,document.body.scrollHeight);

let data_history_chart = [];

let chart_1_visual;
let chart_2_visual;
let chart_3_visual;
let chart_4_visual;

let looser_1_chart;
let looser_2_chart;
let looser_3_chart;
let looser_4_chart;

let gainer_1_chart;
let gainer_2_chart;
let gainer_3_chart;
let gainer_4_chart;

console.log(logos)
console.log(pair_names)

let history_chart;


function getCandleData(data) {
	let chart_data = []
  for (let i = data.length-1; i >= 0; i--) {
	  chart_data.push({
		  time: data[i][0] / 1000,
		  open: data[i][1],
		  high: data[i][2],
		  low: data[i][3],
		  close: data[i][4]
	  });
  }
  console.log(chart_data)
  return chart_data;
}
function getLineData(data) {
	let chart_data = []
  for (let i = data.length-1; i >= 0; i--) {
	  chart_data.push({
		time: data[i][0] / 1000,
		value: data[i][4]
	});
  }
  return chart_data;
}
let markers = [];
let markers2 = [];
let markers3 = [];
let markers4 = [];

const gainers_chartOptions = {
	timeScale:{
		visible: false,
	},
	rightPriceScale:{
		visible: false,
	},
	layout: {
		background: {
			type: 'solid',
			color: '#252d42'
		},
	},
	crosshair: {
        horzLine: {
            visible: false,
        },
		vertLine:{
			visible: false,
		}
    },
	grid: {
        vertLines: {
            visible: false,
        },
        horzLines: {
            visible: false,
        },
    },

	handleScale:false,
	handleScroll:false,
};
const loosers_chartOptions = {
	timeScale:{
		visible: false,
	},
	rightPriceScale:{
		visible: false,
	},
	layout: {
		background: {
			type: 'solid',
			color: '#252d42'
		},
	},
	crosshair: {
        horzLine: {
            visible: false,
        },
		vertLine:{
			visible: false,
		}
    },
	grid: {
        vertLines: {
            visible: false,
        },
        horzLines: {
            visible: false,
        },
    },

	handleScale:false,
	handleScroll:false,
};
const main_chartOptions = {
	timeScale:{
        timeVisible: true,
    },
    rightPriceScale:{
        visible: true,
    },
    handleScale:true,
    handleScroll:true,
    layout: {
        textColor: 'black',
        background: {
            type: 'gradient',
            topColor: "rgba(0,0,0,0)",
            bottomColor: "rgba(0,0,0,0.15)",
        },
    },
};
const history_chartOptions = {
	timeScale:{
		visible: false,
	},
	priceScale:{
		visible: false,
	},
	handleScale:false,
	handleScroll:false,
	layout: {
		textColor: 'black',
		background: {
			type: 'gradient',
			topColor: "rgba(0,0,0,0.6)",
			bottomColor: "rgba(0,0,0,0.75)",
		} ,
	},
}

chart_1_visual = LightweightCharts.createChart(document.getElementById('chart_1_visual'), main_chartOptions);
let candlestickSeries_chart_1_visual = chart_1_visual.addCandlestickSeries();
candlestickSeries_chart_1_visual.setData(getCandleData(chart_1_data));
let sma_10_chart_1_visual = chart_1_visual.addLineSeries()
let sma_20_chart_1_visual = chart_1_visual.addLineSeries()
const chart_1_visual_sma_10 = calculateSMA(getCandleData(chart_1_data), 10);
sma_10_chart_1_visual.setData(chart_1_visual_sma_10);
sma_10_chart_1_visual.applyOptions({
	color: '#3f0072'
})
const chart_1_visual_sma_20 = calculateSMA(getCandleData(chart_1_data), 20);
sma_20_chart_1_visual.setData(chart_1_visual_sma_20);
sma_20_chart_1_visual.applyOptions({
	color: '#588104'
})
candlestickSeries_chart_1_visual.applyOptions({
    priceFormat: {
        type: 'price',
        precision: 6,
        minMove: 0.000001,
    },
});

//ADDING LEGEND
const symbolName = pair_names[0];
const container = document.getElementById('chart_1_visual');
const legend = document.createElement('div');
legend.style = `position: absolute; margin-left: -25%; margin-top: -12%; z-index: 1; font-size: 20px; font-family: sans-serif; line-height: 18px; font-weight: 900;`;
container.appendChild(legend);
const firstRow = document.createElement('div');
firstRow.innerHTML = symbolName;
firstRow.style.color = 'black';
legend.appendChild(firstRow);

//ADDING FUNCTION THAT IS OUTPUTING ACTUAL PRICE OF THE CROSSHAIR NEXT TO THE LEGEND
/*
chart_1_visual.subscribeCrosshairMove(param => {
	let priceFormatted = '';
	if (param.time) {
		const data = param.seriesData(chart_1_visual_areaSeries);
		const price = data.value !== undefined ? data.value : data.close;
		priceFormatted = price.toFixed(2);
	}
	firstRow.innerHTML = `${symbolName} <bold>${priceFormatted}</bold>`;
});*/

chart_2_visual = LightweightCharts.createChart(document.getElementById('chart_2_visual'), main_chartOptions);
let candlestickSeries_chart_2_visual = chart_2_visual.addCandlestickSeries();
candlestickSeries_chart_2_visual.setData(getCandleData(chart_2_data));
let sma_10_chart_2_visual = chart_2_visual.addLineSeries()
let sma_20_chart_2_visual = chart_2_visual.addLineSeries()
const chart_2_visual_sma_10 = calculateSMA(getCandleData(chart_2_data), 10);
sma_10_chart_2_visual.setData(chart_2_visual_sma_10);
sma_10_chart_2_visual.applyOptions({
	color: '#3f0072'
})
const chart_2_visual_sma_20 = calculateSMA(getCandleData(chart_2_data), 20);
sma_20_chart_2_visual.setData(chart_2_visual_sma_20);
sma_20_chart_2_visual.applyOptions({
	color: '#588104'
})
candlestickSeries_chart_2_visual.applyOptions({
    priceFormat: {
        type: 'price',
        precision: 6,
        minMove: 0.000001,
    },
});

const symbolName2 = pair_names[1];
const container2 = document.getElementById('chart_2_visual');
const legend2 = document.createElement('div');
legend2.style = `position: absolute; margin-left: -25%; margin-top: -12%; z-index: 1; font-size: 20px; font-family: sans-serif; line-height: 18px; font-weight: 900;`;
container2.appendChild(legend2);
const firstRow2 = document.createElement('div');
firstRow2.innerHTML = symbolName2;
firstRow2.style.color = 'black';
legend2.appendChild(firstRow2);



chart_3_visual = LightweightCharts.createChart(document.getElementById('chart_3_visual'), main_chartOptions);
let candlestickSeries_chart_3_visual = chart_3_visual.addCandlestickSeries();
candlestickSeries_chart_3_visual.setData(getCandleData(chart_3_data));
let sma_10_chart_3_visual = chart_3_visual.addLineSeries()
let sma_20_chart_3_visual = chart_3_visual.addLineSeries()
const chart_3_visual_sma_10 = calculateSMA(getCandleData(chart_3_data), 10);
sma_10_chart_3_visual.setData(chart_3_visual_sma_10);
sma_10_chart_3_visual.applyOptions({
	color: '#3f0072'
})
const chart_3_visual_sma_20 = calculateSMA(getCandleData(chart_3_data), 20);
sma_20_chart_3_visual.setData(chart_3_visual_sma_20);
sma_20_chart_3_visual.applyOptions({
	color: '#588104'
})
candlestickSeries_chart_3_visual.applyOptions({
    priceFormat: {
        type: 'price',
        precision: 6,
        minMove: 0.000001,
    },
});
const symbolName3 = pair_names[2];
const container3 = document.getElementById('chart_3_visual');
const legend3 = document.createElement('div');
legend3.style = `position: absolute; margin-left: -25%; margin-top: -12%; z-index: 1; font-size: 20px; font-family: sans-serif; line-height: 18px; font-weight: 900;`;
container3.appendChild(legend3);
const firstRow3 = document.createElement('div');
firstRow3.innerHTML = symbolName3;
firstRow3.style.color = 'black';
legend3.appendChild(firstRow3);

chart_4_visual = LightweightCharts.createChart(document.getElementById('chart_4_visual'), main_chartOptions);
let candlestickSeries_chart_4_visual = chart_4_visual.addCandlestickSeries();
candlestickSeries_chart_4_visual.setData(getCandleData(chart_4_data));
let sma_10_chart_4_visual = chart_4_visual.addLineSeries()
let sma_20_chart_4_visual = chart_4_visual.addLineSeries()
const chart_4_visual_sma_10 = calculateSMA(getCandleData(chart_4_data), 10);
sma_10_chart_4_visual.setData(chart_4_visual_sma_10);
sma_10_chart_4_visual.applyOptions({
	color: '#3f0072'
})
const chart_4_visual_sma_20 = calculateSMA(getCandleData(chart_4_data), 20);
sma_20_chart_4_visual.setData(chart_4_visual_sma_20);
sma_20_chart_4_visual.applyOptions({
	color: '#588104'
})
candlestickSeries_chart_4_visual.applyOptions({
    priceFormat: {
        type: 'price',
        precision: 6,
        minMove: 0.000001,
    },
});
const symbolName4 = pair_names[3];
const container4 = document.getElementById('chart_4_visual');
const legend4 = document.createElement('div');
legend4.style = `position: absolute; margin-left: -25%; margin-top: -12%; z-index: 1; font-size: 20px; font-family: sans-serif; line-height: 18px; font-weight: 900;`;
container4.appendChild(legend4);
const firstRow4 = document.createElement('div');
firstRow4.innerHTML = symbolName4;
firstRow4.style.color = 'black';
legend4.appendChild(firstRow4);

gainer_1_chart = LightweightCharts.createChart(document.getElementById('gainer_1_chart'), gainers_chartOptions);
let gainer_1_chart_areaSeries = gainer_1_chart.addAreaSeries({
  topColor: 'rgb(0,255,0)',
  bottomColor: 'rgba(0,255,0,0.1)',
  lineWidth: 2,
  crossHairMarkerVisible: false,
});
gainer_1_chart_areaSeries.setData(getLineData(gainer_1_data));
gainer_1_chart.addLineSeries().setData(getLineData(gainer_1_data));
const symbolName5 = pair_names[4];
const container5 = document.getElementById('gainer_1_chart');
const legend5 = document.createElement('div');
legend5.style = `position: absolute; margin-left: -12%; margin-top: -1%; z-index: 1; font-size: 12px; font-family: sans-serif; line-height: 18px; font-weight: 900;`;
container5.appendChild(legend5);
const firstRow5 = document.createElement('div');
firstRow5.innerHTML = symbolName5;
firstRow5.style.color = 'black';
firstRow5.style.textShadow = '1px 0 0 #FFF, 0 -1px 0 #FFF, 0 1px 0 #FFF, -1px 0 0 #FFF';
legend5.appendChild(firstRow5);

gainer_2_chart = LightweightCharts.createChart(document.getElementById('gainer_2_chart'), gainers_chartOptions);
let gainer_2_chart_areaSeries = gainer_2_chart.addAreaSeries({
  topColor: 'rgb(0,255,0)',
  bottomColor: 'rgba(0,255,0,0.1)',
  lineWidth: 2,
  crossHairMarkerVisible: false,
});
gainer_2_chart_areaSeries.setData(getLineData(gainer_2_data));
gainer_2_chart.addLineSeries().setData(getLineData(gainer_2_data));
const symbolName6 = pair_names[5];
const container6 = document.getElementById('gainer_2_chart');
const legend6 = document.createElement('div');
legend6.style = `position: absolute; margin-left: -12%; margin-top: -1%; z-index: 1; font-size: 12px; font-family: sans-serif; line-height: 18px; font-weight: 900;`;
container6.appendChild(legend6);
const firstRow6 = document.createElement('div');
firstRow6.innerHTML = symbolName6;
firstRow6.style.color = 'black';
firstRow6.style.textShadow = '1px 0 0 #FFF, 0 -1px 0 #FFF, 0 1px 0 #FFF, -1px 0 0 #FFF';
legend6.appendChild(firstRow6);

gainer_3_chart = LightweightCharts.createChart(document.getElementById('gainer_3_chart'), gainers_chartOptions);
let gainer_3_chart_areaSeries = gainer_3_chart.addAreaSeries({
  topColor: 'rgb(0,255,0)',
  bottomColor: 'rgba(0,255,0,0.1)',
  lineWidth: 2,
  crossHairMarkerVisible: false,
});
gainer_3_chart_areaSeries.setData(getLineData(gainer_3_data));
gainer_3_chart.addLineSeries().setData(getLineData(gainer_3_data));
const symbolName7 = pair_names[6];
const container7 = document.getElementById('gainer_3_chart');
const legend7 = document.createElement('div');
legend7.style = `position: absolute; margin-left: -12%; margin-top: -1%; z-index: 1; font-size: 12px; font-family: sans-serif; line-height: 18px; font-weight: 900;`;
container7.appendChild(legend7);
const firstRow7 = document.createElement('div');
firstRow7.innerHTML = symbolName7;
firstRow7.style.color = 'black';
firstRow7.style.textShadow = '1px 0 0 #FFF, 0 -1px 0 #FFF, 0 1px 0 #FFF, -1px 0 0 #FFF';
legend7.appendChild(firstRow7);

gainer_4_chart = LightweightCharts.createChart(document.getElementById('gainer_4_chart'), gainers_chartOptions);
let gainer_4_chart_areaSeries = gainer_4_chart.addAreaSeries({
  topColor: 'rgb(0,255,0)',
  bottomColor: 'rgba(0,255,0,0.1)',
  lineWidth: 2,
  crossHairMarkerVisible: false,
});
gainer_4_chart_areaSeries.setData(getLineData(gainer_4_data));
gainer_4_chart.addLineSeries().setData(getLineData(gainer_4_data));
const symbolName8 = pair_names[7];
const container8 = document.getElementById('gainer_4_chart');
const legend8 = document.createElement('div');
legend8.style = `position: absolute; margin-left: -12%; margin-top: -1%; z-index: 1; font-size: 12px; font-family: sans-serif; line-height: 18px; font-weight: 900;`;
container8.appendChild(legend8);
const firstRow8 = document.createElement('div');
firstRow8.innerHTML = symbolName8;
firstRow8.style.color = 'black';
firstRow8.style.textShadow = '1px 0 0 #FFF, 0 -1px 0 #FFF, 0 1px 0 #FFF, -1px 0 0 #FFF';
legend8.appendChild(firstRow8);

looser_1_chart = LightweightCharts.createChart(document.getElementById('looser_1_chart'), loosers_chartOptions);
let looser_1_chart_areaSeries = looser_1_chart.addAreaSeries({
  topColor: 'rgb(255,0,0)',
  bottomColor: 'rgba(255,0,0,0.1)',
  lineWidth: 2,
  crossHairMarkerVisible: false,
});
looser_1_chart_areaSeries.setData(getLineData(looser_1_data));
looser_1_chart.addLineSeries().setData(getLineData(looser_1_data));
const symbolName9 = pair_names[8];
const container9 = document.getElementById('looser_1_chart');
const legend9 = document.createElement('div');
legend9.style = `position: absolute; margin-left: 12%; margin-top: -1%; z-index: 1; font-size: 12px; font-family: sans-serif; line-height: 18px; font-weight: 900;`;
container9.appendChild(legend9);
const firstRow9 = document.createElement('div');
firstRow9.innerHTML = symbolName9;
firstRow9.style.color = 'black';
firstRow9.style.textShadow = '1px 0 0 #FFF, 0 -1px 0 #FFF, 0 1px 0 #FFF, -1px 0 0 #FFF';
legend9.appendChild(firstRow9);

looser_2_chart = LightweightCharts.createChart(document.getElementById('looser_2_chart'), loosers_chartOptions);
let looser_2_chart_areaSeries = looser_2_chart.addAreaSeries({
  topColor: 'rgb(255,0,0)',
  bottomColor: 'rgba(255,0,0,0.1)',
  lineWidth: 2,
  crossHairMarkerVisible: false,
});
looser_2_chart_areaSeries.setData(getLineData(looser_2_data));
looser_2_chart.addLineSeries().setData(getLineData(looser_2_data));
const symbolName10 = pair_names[9];
const container10 = document.getElementById('looser_2_chart');
const legend10 = document.createElement('div');
legend10.style = `position: absolute; margin-left: 12%; margin-top: -1%; z-index: 1; font-size: 12px; font-family: sans-serif; line-height: 18px; font-weight: 900;`;
container10.appendChild(legend10);
const firstRow10 = document.createElement('div');
firstRow10.innerHTML = symbolName10;
firstRow10.style.color = 'black';
firstRow10.style.textShadow = '1px 0 0 #FFF, 0 -1px 0 #FFF, 0 1px 0 #FFF, -1px 0 0 #FFF';
legend10.appendChild(firstRow10);

looser_3_chart = LightweightCharts.createChart(document.getElementById('looser_3_chart'), loosers_chartOptions);
let looser_3_chart_areaSeries = looser_3_chart.addAreaSeries({
  topColor: 'rgb(255,0,0)',
  bottomColor: 'rgba(255,0,0,0.1)',
  lineWidth: 2,
  crossHairMarkerVisible: false,
});
looser_3_chart_areaSeries.setData(getLineData(looser_3_data));
looser_3_chart.addLineSeries().setData(getLineData(looser_3_data));
const symbolName11 = pair_names[10];
const container11 = document.getElementById('looser_3_chart');
const legend11 = document.createElement('div');
legend11.style = `position: absolute; margin-left: 12%; margin-top: -1%; z-index: 1; font-size: 12px; font-family: sans-serif; line-height: 18px; font-weight: 900;`;
container11.appendChild(legend11);
const firstRow11 = document.createElement('div');
firstRow11.innerHTML = symbolName11;
firstRow11.style.color = 'black';
firstRow11.style.textShadow = '1px 0 0 #FFF, 0 -1px 0 #FFF, 0 1px 0 #FFF, -1px 0 0 #FFF';
legend11.appendChild(firstRow11);

looser_4_chart = LightweightCharts.createChart(document.getElementById('looser_4_chart'), loosers_chartOptions);
let looser_4_chart_areaSeries = looser_4_chart.addAreaSeries({
  topColor: 'rgb(255,0,0)',
  bottomColor: 'rgba(255,0,0,0.1)',
  lineWidth: 2,
  crossHairMarkerVisible: false,
});
looser_4_chart_areaSeries.setData(getLineData(looser_4_data));
looser_4_chart.addLineSeries().setData(getLineData(looser_4_data));
const symbolName12 = pair_names[11];
const container12 = document.getElementById('looser_4_chart');
const legend12 = document.createElement('div');
legend12.style = `position: absolute; margin-left: 12%; margin-top: -1%; z-index: 1; font-size: 12px; font-family: sans-serif; line-height: 18px; font-weight: 900;`;
container12.appendChild(legend12);
const firstRow12 = document.createElement('div');
firstRow12.innerHTML = symbolName12;
firstRow12.style.color = 'black';
firstRow12.style.textShadow = '1px 0 0 #FFF, 0 -1px 0 #FFF, 0 1px 0 #FFF, -1px 0 0 #FFF';
legend12.appendChild(firstRow12);

history_chart = LightweightCharts.createChart(document.getElementById('history_chart'), history_chartOptions);
history_chart.addLineSeries().setData(data_history_chart);

const checkbox_ma_10_chart_1 = document.querySelector('input[name="ma_10_chart_1"]');
const checkbox_ma_10_chart_2 = document.querySelector('input[name="ma_10_chart_2"]');
const checkbox_ma_10_chart_3 = document.querySelector('input[name="ma_10_chart_3"]');
const checkbox_ma_10_chart_4 = document.querySelector('input[name="ma_10_chart_4"]');
const checkbox_ma_20_chart_1 = document.querySelector('input[name="ma_20_chart_1"]');
const checkbox_ma_20_chart_2 = document.querySelector('input[name="ma_20_chart_2"]');
const checkbox_ma_20_chart_3 = document.querySelector('input[name="ma_20_chart_3"]');
const checkbox_ma_20_chart_4 = document.querySelector('input[name="ma_20_chart_4"]');

checkbox_ma_10_chart_1.addEventListener('click', function() {
  const isChecked = checkbox_ma_10_chart_1.checked;
  if (isChecked) {
	  sma_10_chart_1_visual.applyOptions({
      visible: true
    });
  }
  else {
	  sma_10_chart_1_visual.applyOptions({
      visible: false
    });
  }
});
checkbox_ma_10_chart_2.addEventListener('click', function() {
  const isChecked = checkbox_ma_10_chart_2.checked;
  if (isChecked) {
	  sma_10_chart_2_visual.applyOptions({
      visible: true
    });
  }
  else {
	  sma_10_chart_2_visual.applyOptions({
      visible: false
    });
  }
});
checkbox_ma_10_chart_3.addEventListener('click', function() {
  const isChecked = checkbox_ma_10_chart_3.checked;
  if (isChecked) {
	  sma_10_chart_3_visual.applyOptions({
      visible: true
    });
  }
  else {
	  sma_10_chart_3_visual.applyOptions({
      visible: false
    });
  }
});
checkbox_ma_10_chart_4.addEventListener('click', function() {
  const isChecked = checkbox_ma_10_chart_4.checked;
  if (isChecked) {
	  sma_10_chart_4_visual.applyOptions({
      visible: true
    });
  }
  else {
	  sma_10_chart_4_visual.applyOptions({
      visible: false
    });
  }
});
checkbox_ma_20_chart_1.addEventListener('click', function() {
  const isChecked = checkbox_ma_20_chart_1.checked;
  if (isChecked) {
	  sma_20_chart_1_visual.applyOptions({
      visible: true
    });
  }
  else {
	  sma_20_chart_1_visual.applyOptions({
      visible: false
    });
  }
});
checkbox_ma_20_chart_2.addEventListener('click', function() {
  const isChecked = checkbox_ma_20_chart_2.checked;
  if (isChecked) {
	  sma_20_chart_2_visual.applyOptions({
      visible: true
    });
  }
  else {
	  sma_20_chart_2_visual.applyOptions({
      visible: false
    });
  }
});
checkbox_ma_20_chart_3.addEventListener('click', function() {
  const isChecked = checkbox_ma_20_chart_3.checked;
  if (isChecked) {
	  sma_20_chart_3_visual.applyOptions({
      visible: true
    });
  }
  else {
	  sma_20_chart_3_visual.applyOptions({
      visible: false
    });
  }
});
checkbox_ma_20_chart_4.addEventListener('click', function() {
  const isChecked = checkbox_ma_20_chart_4.checked;
  if (isChecked) {
	  sma_20_chart_4_visual.applyOptions({
      visible: true
    });
  }
  else {
	  sma_20_chart_4_visual.applyOptions({
      visible: false
    });
  }
});



//PEAK VALLEYS
for (let i = peak_valley_begin.length-1; i >= 0; i--) {
	if (peak_valley_type[i] === 0) {
		markers.push({
			time: peak_valley_begin[i],
			position: 'aboveBar',
			color: '#e91e63',
			shape: 'arrowDown',
			text: 'Sell',
            size: 3,
		});
	}
    else {
		markers.push({
			time: peak_valley_begin[i],
			position: 'belowBar',
			color: '#2196F3',
			shape: 'arrowUp',
			text: 'Buy',
            size: 3,
		});
	}
}
markers = markers.sort((a,b) => (a.time - b.time));
candlestickSeries_chart_1_visual.setMarkers(markers);
chart_1_visual.timeScale().fitContent();

for (let i = peak_valley_begin2.length-1; i >= 0; i--) {
	if (peak_valley_type2[i] === 0) {
		markers2.push({
			time: peak_valley_begin2[i],
			position: 'aboveBar',
			color: '#e91e63',
			shape: 'arrowDown',
			text: 'Sell',
            size: 3,
		});
	}
    else {
		markers2.push({
			time: peak_valley_begin2[i],
			position: 'belowBar',
			color: '#2196F3',
			shape: 'arrowUp',
			text: 'Buy',
            size: 3,
		});
	}
}
markers2 = markers2.sort((a,b) => (a.time - b.time));
candlestickSeries_chart_2_visual.setMarkers(markers2);
chart_2_visual.timeScale().fitContent();

for (let i = peak_valley_begin3.length-1; i >= 0; i--) {
	if (peak_valley_type3[i] === 0) {
		markers3.push({
			time: peak_valley_begin3[i],
			position: 'aboveBar',
			color: '#e91e63',
			shape: 'arrowDown',
			text: 'Sell',
            size: 3,
		});
	}
    else {
		markers3.push({
			time: peak_valley_begin3[i],
			position: 'belowBar',
			color: '#2196F3',
			shape: 'arrowUp',
			text: 'Buy',
            size: 3,
		});
	}
}
markers3 = markers3.sort((a,b) => (a.time - b.time));
candlestickSeries_chart_3_visual.setMarkers(markers3);
chart_3_visual.timeScale().fitContent();

for (let i = peak_valley_begin4.length-1; i >= 0; i--) {
	if (peak_valley_type4[i] === 0) {
		markers4.push({
			time: peak_valley_begin4[i],
			position: 'aboveBar',
			color: '#e91e63',
			shape: 'arrowDown',
			text: 'Sell',
            size: 3,
		});
	}
    else {
		markers4.push({
			time: peak_valley_begin4[i],
			position: 'belowBar',
			color: '#2196F3',
			shape: 'arrowUp',
			text: 'Buy',
            size: 3,
		});
	}
}
markers4 = markers4.sort((a,b) => (a.time - b.time));
candlestickSeries_chart_4_visual.setMarkers(markers4);
chart_4_visual.timeScale().fitContent();

//Logos of gainers and loosers
{
	let gainer_1_logo = document.createElement("img");
	gainer_1_logo.id = "gainer"
	gainer_1_logo.src = logos[0];
	let gainer_1_logo_right = document.createElement("img");
	gainer_1_logo_right.id = "gainer"
	gainer_1_logo_right.src = logos[1];
	let src = document.getElementById("gainer_1_logo");
	src.appendChild(gainer_1_logo);
	src.appendChild(gainer_1_logo_right);

	let gainer_2_logo = document.createElement("img");
	gainer_2_logo.id = "gainer"
	gainer_2_logo.src = logos[2];
	let gainer_2_logo_right = document.createElement("img");
	gainer_2_logo_right.id = "gainer"
	gainer_2_logo_right.src = logos[3];
	let src1 = document.getElementById("gainer_2_logo");
	src1.appendChild(gainer_2_logo);
	src1.appendChild(gainer_2_logo_right);

	let gainer_3_logo = document.createElement("img");
	gainer_3_logo.id = "gainer"
	gainer_3_logo.src = logos[4];
	let gainer_3_logo_right = document.createElement("img");
	gainer_3_logo_right.id = "gainer"
	gainer_3_logo_right.src = logos[5];
	let src2 = document.getElementById("gainer_3_logo");
	src2.appendChild(gainer_3_logo);
	src2.appendChild(gainer_3_logo_right);

	let gainer_4_logo = document.createElement("img");
	gainer_4_logo.id = "gainer"
	gainer_4_logo.src = logos[6];
	let gainer_4_logo_right = document.createElement("img");
	gainer_4_logo_right.id = "gainer"
	gainer_4_logo_right.src = logos[7];
	let src3 = document.getElementById("gainer_4_logo");
	src3.appendChild(gainer_4_logo);
	src3.appendChild(gainer_4_logo_right);

	let looser_1_logo = document.createElement("img");
	looser_1_logo.id = "looser"
	looser_1_logo.src = logos[8];
	let looser_1_logo_right = document.createElement("img");
	looser_1_logo_right.id = "looser"
	looser_1_logo_right.src = logos[9];
	let src4 = document.getElementById("looser_1_logo");
	src4.appendChild(looser_1_logo);
	src4.appendChild(looser_1_logo_right);

	let looser_2_logo = document.createElement("img");
	looser_2_logo.id = "looser"
	looser_2_logo.src = logos[10];
	let looser_2_logo_right = document.createElement("img");
	looser_2_logo_right.id = "looser"
	looser_2_logo_right.src = logos[11];
	let src5 = document.getElementById("looser_2_logo");
	src5.appendChild(looser_2_logo);
	src5.appendChild(looser_2_logo_right);

	let looser_3_logo = document.createElement("img");
	looser_3_logo.id = "looser"
	looser_3_logo.src = logos[12];
	let looser_3_logo_right = document.createElement("img");
	looser_3_logo_right.id = "looser"
	looser_3_logo_right.src = logos[13];
	let src6 = document.getElementById("looser_3_logo");
	src6.appendChild(looser_3_logo);
	src6.appendChild(looser_3_logo_right);

	let looser_4_logo = document.createElement("img");
	looser_4_logo.id = "looser"
	looser_4_logo.src = logos[14];
	let looser_4_logo_right = document.createElement("img");
	looser_4_logo_right.id = "looser"
	looser_4_logo_right.src = logos[15];
	let src7 = document.getElementById("looser_4_logo");
	src7.appendChild(looser_4_logo);
	src7.appendChild(looser_4_logo_right);
}

function calculateSMA(data, count) {
  function calculateAverage(dataArray) {
    let sum = 0;
    for (let i = 0; i < dataArray.length; i++) {
      sum += dataArray[i].close;
    }
    return sum / dataArray.length;
  }

  const smaResult = [];

  for (let i = count - 1, dataLength = data.length; i < dataLength; i++) {
    const slicedData = data.slice(i - count + 1, i);
    const smaValue = calculateAverage(slicedData);
    smaResult.push({time: data[i].time, value: smaValue});
  }

  return smaResult;
}
